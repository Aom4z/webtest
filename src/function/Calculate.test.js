const { addiction, subtraction, total_calculate } = require('./Calculate.js');

test('addiction : ',()=>{
    expect(addiction(3,5)).toBe(8);
});

test('subtraction : ',()=>{
    expect(subtraction(5,3)).toBe(2);
});

describe('test', () => {
    test('Calculate add and subtraction', () => {
        const a = 10;
        const b = 100;
        const c = 50;
        const d = 20;
        const add = addiction(a, b);
        const sub = subtraction(c, d);
        const total = total_calculate(add,sub);

        expect(add).toBe(110)
        expect(sub).toBe(30)
        expect(total).toBe(70)
    });
});

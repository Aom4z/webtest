function addiction(a, b){  
    return a + b;
  }
    
  function subtraction(a, b){  
    return a - b;
  }
    
  function total_calculate(add,sub) {
    return (add + sub)/2;
  }
    
    
  module.exports = {
    addiction,subtraction,total_calculate
  };
  
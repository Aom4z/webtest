const { total_calculate } = require('./Calculate.js');

describe('test', () => {
    test('Calculate add and subtraction', () => {
        const add = 8
        const sub = 2
        // const add = 5
        // const sub = 3
        const total = total_calculate(add,sub);

        expect(add).toBe(8)
        expect(sub).toBe(2)
        expect(total).toBe(5)
    });
});
